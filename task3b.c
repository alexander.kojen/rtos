static void prvCopyDataFromQueue( xQUEUE * const pxQueue, const void *pvBuffer, portBASE_TYPE hilo )
{
    void *buffer = NULL; //replace holder
    portBASE_TYPE i;
    
    if(hilo == 1){
        char id = -1; // do it as low as possible
    	if( pxQueue->uxQueueType != queueQUEUE_IS_MUTEX )
    	{
    	    for(i=0, pxQueue->pcReadFrom = pxQueue->pcHead; i < pxQueue->uxLength ;i++, (pxQueue->pcReadFrom + pxQueue->uxItemSize)){
    	        if( *(char *)pxQueue->pcReadFrom >= id ){
    	            buffer = pxQueue->pcReadFrom; //vielleicht (void *) pxQueue->pcReadFrom;
    	        }
    	    }
    		memcpy( ( void * ) pvBuffer, ( void * ) buffer, ( unsigned ) pxQueue->uxItemSize ); //copy highes priority
    	}
        
    }
    else{
        char id = 127; //do it as high as possible
	    if( pxQueue->uxQueueType != queueQUEUE_IS_MUTEX )
	    {
	        for(i=0, pxQueue->pcReadFrom = pxQueue->pcHead; i < pxQueue->uxLength ;i++, (pxQueue->pcReadFrom + pxQueue->uxItemSize)){
	            if( *(char *)pxQueue->pcReadFrom <= id ){
	             buffer = pxQueue->pcReadFrom; //vielleicht (void *) pxQueue->pcReadFrom;
	            }
	        }
		    memcpy( ( void * ) pvBuffer, ( void * ) buffer, ( unsigned ) pxQueue->uxItemSize ); //copy lowest priority
	    }
        
    }

}
