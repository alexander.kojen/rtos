#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#include "Drivers/interrupts.h"
#include "Drivers/gpio.h"
#include "Drivers/print_473.h"

//portTickType time = 0;
//portTickType state = 0;
//char input = 0;
//char button = 0;
//xTaskHandle timeHandle = NULL;
//xTaskHandle displayHandle = NULL;
//xTaskHandle inputHandle = NULL;
const portTickType xDelay1 = 100; //100 mSek
const portTickType xDelay2 = 200; //200 mSek
const portTickType xDelay3 = 300; //300 mSek

//portTickType xLastWakeTime = 0;

//unsigned portBASE_TYPE uxQueueLength = 0;
//unsigned portBASE_TYPE uxItemSize = 0;
//char str[10] = NULL;
//portTickType queue = 0;


xTaskHandle prod1Handle = NULL;
xTaskHandle prod2Handle = NULL;
xTaskHandle prod3Handle = NULL;
xTaskHandle printHandle = NULL;

xQueueHandle printQueue = NULL;

void myProd1Task(void *pParam);
void myProd2Task(void *pParam);
void myProd3Task(void *pParam);

void myPrintTask(void *pParam);

//portTickType startTick = 0;
//portTickType oldTick = 0;
//portTickType clearedTick = 0;

/*struct Message
{
	char messageID;
	char data[ 20 ];
} pMessage;*/


void myProd1Task(void *pParam) {

	/*struct Message *pMessage;
	pMessage = ( struct Message * ) pvPortMalloc( sizeof( struct Message* ) );
	pMessage->messageID = 1;
	pMessage->data = "printedone"*/
	
	//uint32t dataToSend = 1;
	portTickType dataToSend = 1;
	for( ; ; ){
		vTaskDelay(xDelay1);
		//xQueueSend( printQueue, pMessage, (portTickType)0 );

		xQueueSend( printQueue, &dataToSend, (portTickType)0 );
		
		

	}
}

void myProd2Task(void *pParam) {
	//button = uart_getc()
	
	//uint32t dataToSend = 2;
	portTickType dataToSend = 2;
	for( ; ; ){
		vTaskDelay(xDelay2);
		xQueueSend( printQueue, &dataToSend, (portTickType)0 );
		

		//uart_putc ('y');
			
			
	}
}

//void myTimingTask(void *pParam) {
void myProd3Task(void *pParam) {

	//uart_putc ('n');
	//uint32t dataToSend = 3;
	portTickType dataToSend = 3;
	//xLastWakeTime = xTaskGetTickCount();
	for( ; ; ){
		vTaskDelay(xDelay3);
		xQueueSend( printQueue, &dataToSend, (portTickType)0 );
		
		
		//uart_putc ('z');
		//vTaskDelayUntil( &xLastWakeTime, xDelay );
		

		//vTaskDelay(xDelay); 
		//vTaskDelayUntil( &xLastWakeTime, xDelay );
	}
}

void myPrintTask(void *pParam) {

	portTickType dataToReceive = 0;
	//uart_putc ('n');

	//xLastWakeTime = xTaskGetTickCount();
	for( ; ; ){
		if( xQueueReceive( printQueue, &dataToReceive, (portTickType)0 )){
			//vTaskDelay(100); 
			

			if((dataToReceive) == 1){
				printf_473("Product 1 is printed\n");
			}

			else if((dataToReceive) == 2){
				printf_473("Product 2 is printed\n");
			}
			else if((dataToReceive) == 3){
				printf_473("Product 3 is printed\n");
			}
			else{
				printf_473("Mistake\n");
			}
		}
		//uart_putc ('z');
		//vTaskDelayUntil( &xLastWakeTime, xDelay );	

		//vTaskDelay(xDelay); 
		//vTaskDelayUntil( &xLastWakeTime, xDelay );
	}
}

int main(void) {
	
	//SetGpioFunction(11, 1); //Initialize the Button

	uart_init (); //Initialize the Button
	
	printQueue = xQueueCreate( 5, (unsigned portBASE_TYPE)sizeof(unsigned portSHORT) );

	/* Create a taskS*/
	/* "Each task is assigned a priority from 0 to ( configMAX_PRIORITIES - 1 ),
		where configMAX_PRIORITIES is defined within FreeRTOSConfig.h.
		-----> Hier: 5, d.h. maxPrio = 4
	*/
	xTaskCreate(myProd1Task, "Prod1", 128, NULL, 2, &prod1Handle);
	xTaskCreate(myProd2Task, "Prod2", 128, NULL, 2, &prod2Handle);
	xTaskCreate(myProd3Task, "Prod3", 128, NULL, 2, &prod3Handle);
	
	xTaskCreate(myPrintTask, "Printer", 128, NULL, 1, &printHandle);	


	/* Start the scheduler to start the tasks executing. */
	//startTick = xTaskGetTickCount();
	vTaskStartScheduler();

	/* The following line should never be reached because vTaskStartScheduler() 
	will only return if there was not enough FreeRTOS heap memory available to
	create the Idle and (if configured) Timer tasks.  Heap management, and
	techniques for trapping heap exhaustion, are described in the book text. */
	for( ;; );
	return 0;
}
