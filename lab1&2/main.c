#include <FreeRTOS.h>
#include <task.h>

#include "Drivers/interrupts.h"
#include "Drivers/gpio.h"

const portTickType time = 0;
const portTickType state = 0;
const portTickType button = 0;
TaskHandle_t xHandle;
const TickType_t xDelay = 1000 //1 Sek

void myDisplayTask(void *pParam) {
	for( ;; ){
		uart_putc (time);
	}
}

void myInputTask(void *pParam) {
	/*
	https://www.freertos.org/a00130.html
	while (state == 0): //Ready state
		if (button 1):
			myTimingTask('go') //Starte timer
			state = 1 //Läuft
			
	while (state == 1): //Läuft state
		if (button 1):
			vTaskSuspend( xHandle );
			state = 2 //Paused
			
		if (button 2): //Reset, doppelclick o.Ä.
			vTaskDelete( xHandle );
			state = 0 //Ready
		
	while (state == 2): //Paused state
		if (button 1):
			vTaskResume( xHandle );
			state = 1 //Läuft
			
		if (button 2):
			vTaskDelete( xHandle );
			state = 0 //Ready	
	*/	
}

void myTimingTask(void *pParam) {
		// https://www.freertos.org/FreeRTOS_Support_Forum_Archive/January_2012/freertos_time_measure_4976822.html		
	xTickType oldTick
	for( ;; ){
		oldTick = xTaskGetTickCount();
		vTaskDelay(xDelay);
		time = xTaskGetTickCount() - oldTick; //Amount of seconds passed
	}	
}

int main(void) {
	
	SetGpioFunction(11, 1); //Initialize the Button

	uart_init (); //Initialize the Button
	

	/* Create a taskS*/
	/* "Each task is assigned a priority from 0 to ( configMAX_PRIORITIES - 1 ),
		where configMAX_PRIORITIES is defined within FreeRTOSConfig.h.
		-----> Hier: 5, d.h. maxPrio = 4
	*/
	xTaskCreate(myDisplayTask, "Display", 128, NULL, 1, NULL);
	xTaskCreate(myInputTask, "Input", 128, NULL, 2, NULL);
	xTaskCreate(myTimingTask, "Timing", 128, NULL, 4, &xHandle);	


	/* Start the scheduler to start the tasks executing. */
	vTaskStartScheduler();

	/* The following line should never be reached because vTaskStartScheduler() 
	will only return if there was not enough FreeRTOS heap memory available to
	create the Idle and (if configured) Timer tasks.  Heap management, and
	techniques for trapping heap exhaustion, are described in the book text. */
	for( ;; );
	return 0;
}
