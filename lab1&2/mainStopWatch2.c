#include <FreeRTOS.h>
#include <task.h>

#include "Drivers/interrupts.h"
#include "Drivers/gpio.h"
#include "Drivers/print_473.h"

portTickType time = 0;
portTickType state = 0;
char button = 0;
xTaskHandle timeHandle = NULL;
xTaskHandle displayHandle = NULL;
xTaskHandle inputHandle = NULL;
const portTickType xDelay = 1000; //1 Sek
portTickType xLastWakeTime = 0;



void myDisplayTask(void *pParam);
void myInputTask(void *pParam);
void myTimingTask(void *pParam);

portTickType startTick = 0;
portTickType oldTick = 0;
portTickType clearedTick = 0;


void myDisplayTask(void *pParam) {
	if( state ==2){

		uart_putc ('t');

		//printf_473("%d\n", time/1000);
		printf_473("%d\n", time);
		//myInputTask(inputHandle);
	}
}

void myInputTask(void *pParam) {
	//button = uart_getc()
	while(1){

		uart_putc ('a');

		while (state == 0){ //Ready state

			uart_putc ('b');

			if (uart_getc() == 's'){

				//uart_putc ('c');

				state = 1;
				//myTimingTask(timeHandle); //Starte timer
				

				uart_putc ('d');
				//state = 1; //Läuft
				break;
				}
			
		}
			
		while (state == 1){ //Läuft state

			uart_putc ('e');

			if (uart_getc() == 'p'){

				uart_putc ('f');

				state = 2; //Paused
				//myTimingTask(timeHandle);
				//myDisplayTask(displayHandle);

				uart_putc ('h');

				break;
			}
			/*if (uart_getc() == 'c'){ //Reset, doppelclick o.Ä.
				//vTaskDelete( timeHandle );
				uart_putc ('i');
				state = 0; //Ready
				break;
			}*/
		}
		while (state == 2){ //Paused state

			uart_putc ('j');
			
			if (uart_getc() == 's'){

				uart_putc ('k');

				state = 1; //Läuft
				break;
			}			
			if (uart_getc() == 'c'){
				
				//time = 0;
				uart_putc ('m');
				//vTaskDelete( timeHandle );
				state = 0; //Ready
				break;
			}
		}	
			
	}
}

//void myTimingTask(void *pParam) {
void myTimingTask(void *pParam) {

	uart_putc ('n');

	xLastWakeTime = xTaskGetTickCount();
	
/*		if (state == 1) {
			oldTick = xTaskGetTickCount();
			
			 //Amount of seconds passed
		}
		if( state == 2){

			time = xTaskGetTickCount() - (oldTick - startTick - pausedTick);
			myDisplayTask(displayHandle);
			//(vTaskSuspend( timeHandle );
		}*/
		
		//myInputTask(inputHandle);
		
		// 
		if (state == 0){
			time = 0;
		}		

		if (state == 1){
			time = time + 1;
		}
		vTaskDelay(xDelay); 
		//vTaskDelayUntil( &xLastWakeTime, xDelay );
}

int main(void) {
	
	SetGpioFunction(11, 1); //Initialize the Button

	uart_init (); //Initialize the Button
	

	/* Create a taskS*/
	/* "Each task is assigned a priority from 0 to ( configMAX_PRIORITIES - 1 ),
		where configMAX_PRIORITIES is defined within FreeRTOSConfig.h.
		-----> Hier: 5, d.h. maxPrio = 4
	*/
	xTaskCreate(myDisplayTask, "Display", 128, NULL, 1, &displayHandle);
	xTaskCreate(myInputTask, "Input", 128, NULL, 1, NULL);
	xTaskCreate(myTimingTask, "Timing", 128, NULL, 2, &timeHandle);	


	/* Start the scheduler to start the tasks executing. */
	//startTick = xTaskGetTickCount();
	vTaskStartScheduler();

	/* The following line should never be reached because vTaskStartScheduler() 
	will only return if there was not enough FreeRTOS heap memory available to
	create the Idle and (if configured) Timer tasks.  Heap management, and
	techniques for trapping heap exhaustion, are described in the book text. */
	for( ;; );
	return 0;
}
