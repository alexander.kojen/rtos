#include <FreeRTOS.h>
#include <task.h>

#include "Drivers/interrupts.h"
#include "Drivers/gpio.h"
#include "Drivers/print_473.h"

portTickType time = 0;
portTickType state = 0;
//char button = 0;
//xTaskHandle timeHandle = NULL;
//xTaskHandle displayHandle = NULL;
//xTaskHandle inputHandle = NULL;
const portTickType xDelay = 1000; //1 Sek
portTickType xLastWakeTime = 0;



void myDisplayTask(void *pParam);
void myInputTask(void *pParam);
void myTimingTask(void *pParam);

//portTickType startTick = 0;
//portTickType oldTick = 0;
//portTickType clearedTick = 0;


void myDisplayTask(void *pParam) {

	for( ; ; ){
		
		//die Zeit die wir messen soll nur ausgegeben werden, wenn wir mit p die Zeit stoppen (dann befinden wir uns im state 2).
		if( state ==2){
			uart_putc ('t');
			printf_473("%d\n", time);
		}
	}
}

void myInputTask(void *pParam) {
	//button = uart_getc()
	for( ; ; ){

		uart_putc ('a');

		while (state == 0){ //Ready state

			uart_putc ('b');

			// bereit die StopWatch zu starten:) und in state 1 überzugehen. Vorher state 0.
			if (uart_getc() == 's'){
				state = 1;

				uart_putc ('d');

				break;
				}
			
		}
			
		while (state == 1){ //Läuft state

			uart_putc ('e');
			
			//falls die StopWatch durch p nicht unterbrochen wird, läuft die StopWatch und befindet sich im state 1. Nach Eingabe von p wird die StopWatch gestoppt und es wird in state 2 	           //übergegangen.
			if (uart_getc() == 'p'){

				uart_putc ('f');

				state = 2; //Paused

				uart_putc ('h');

				break;
			}
			/*if (uart_getc() == 'c'){ //Reset, doppelclick o.Ä.
				//vTaskDelete( timeHandle );
				uart_putc ('i');
				state = 0; //Ready
				break;
			}*/
		}
		while (state == 2){ //Paused state
			// im pausierten state 2 wartet die StopWatch darauf mit s fortgesetzt, um in den state 1 überzugehen oder mit c gecleared zu werden, um in den state 0 überzugehen
			uart_putc ('j');
			
			if (uart_getc() == 's'){

				uart_putc ('k');

				state = 1; //Läuft
				break;
			}			
			if (uart_getc() == 'c'){
				
				uart_putc ('m');

				state = 0; //Ready
				break;
			}
		}	
			
	}
}

void myTimingTask(void *pParam) {

	uart_putc ('n');

	//steht so in der API, dass man xLastWakeTime in Kombination mit xTaskDelayUntil verwenden muss
	//xLastWakeTime = xTaskGetTickCount();
	for( ; ; ){
		//falls TaskDelayUntil vermutlich am Anfang der for loop, aber kein Plan, vielleicht auch am Ende.
		//vTaskDelayUntil( &xLastWakeTime, xDelay );
 
		//immer wenn er im state 1 also running ist (in diesem sollte er sein, wenn wir die StopWatch mit s aktiviert haben), soll er nach jeder Sekunde einfach eine Sekunde draufzählen
		if (state == 1){
			time = time + 1;
		}

		//immer wenn wir mit c die StopWatch clearen, dann soll er beim Nächsten mal wieder bei 0 beginnen.
		if (state == 0){
			time = 0;
		}
		
		// hier rufen wir vTaskDelay auf, damit die Task periodisch nur jede Sekunde aufgerufen wird
		vTaskDelay(xDelay); 
		//vTaskDelayUntil( &xLastWakeTime, xDelay );
	}
}

int main(void) {
	
	SetGpioFunction(11, 1); //Initialize the Button

	uart_init (); //Initialize the Button
	

	/* Create a taskS*/
	/* "Each task is assigned a priority from 0 to ( configMAX_PRIORITIES - 1 ),
		where configMAX_PRIORITIES is defined within FreeRTOSConfig.h.
		-----> Hier: 5, d.h. maxPrio = 4
	*/
	xTaskCreate(myDisplayTask, "Display", 128, NULL, 1, &displayHandle);
	xTaskCreate(myInputTask, "Input", 128, NULL, 1, NULL);
	xTaskCreate(myTimingTask, "Timing", 128, NULL, 2, &timeHandle);	


	/* Start the scheduler to start the tasks executing. */
	//startTick = xTaskGetTickCount();
	vTaskStartScheduler();

	/* The following line should never be reached because vTaskStartScheduler() 
	will only return if there was not enough FreeRTOS heap memory available to
	create the Idle and (if configured) Timer tasks.  Heap management, and
	techniques for trapping heap exhaustion, are described in the book text. */
	for( ;; );
	return 0;
}
